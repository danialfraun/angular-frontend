import { Injectable } from '@angular/core';
import {
  HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor,
  HttpRequest,
  HttpResponse
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environment/environment';
import { apiConfig } from 'src/app/config/api-config';

@Injectable({
  providedIn: 'root'
})
export class HttpInterceptorService implements HttpInterceptor{

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    // if the system is not in production, change the bearer for each http request
    if (!environment.production) {
      const modifiedReq = req.clone({ 
        headers: req.headers.set('Authorization', `Bearer ${apiConfig.token}`),
      });
      return next.handle(modifiedReq);
    }
    
    // if there is nothing to intercept, pass the request.
    return next.handle(req);
  }
}
