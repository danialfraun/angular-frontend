import { environment } from "src/environment/environment";

export const apiConfig = {
    
    apiBaseUrl: environment.baseURL,
    // eslint-disable-next-line max-len
    token: '',
    catalog:{
        getCatalog:{
            'api-url': environment.backendURL + 'catalog',
        }
    }
}